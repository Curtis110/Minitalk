/*
** progs.c for progs Minitalk 2 in /home/wurfl_v/rendu/PSU_2013_minitalk
** 
** Made by a
** Login   <wurfl_v@epitech.net>
** 
** Started on  Mon Mar  3 19:50:39 2014 a
** Last update Sun Mar 23 03:07:28 2014 a
*/

#include <unistd.h>

void	my_putchar(char c)
{
  write(1, &c, 1);
}

void	my_putstr(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      my_putchar(str[i]);
      i++;
    }
}

int	my_strlen(char *str)
{
  int	i;

  i = 0;
  while (str[i] != '\0')
    {
      i++;
    }
  return(i);
}

void	my_put_nbr(int nb)
{
  int	var;
  int	pow;

  pow = 1;
  if (nb < 0)
    {
      my_putchar(52);
      nb = nb * -1;
    }
  while (nb >= pow)
    {
      pow = pow * 10;
    }
  pow = pow / 10;
  while (pow > 0)
    {
      var = nb / pow;
      var = var + 48;
      my_putchar(var);
      nb = nb % pow;
      pow = pow / 10;
    }
}
 
int	my_getnbr(char *str)
{
  int	result;
  int	j;
  int	i;

  result = 0;
  j = 1;
  i = 0;
  while (str[i] < '0' || str[i] > '9')
    {
      i = i + 1;
    }
  if (str[i - 1] == '-')
    {
      j = -1;
    }
  while (str[i] >= '0' && str[i] <= '9')
    {
      result = result * 10;
      result = result + (str[i] - 48);
      i = i + 1;
    }
  result = result * j;
  return (result);
}
