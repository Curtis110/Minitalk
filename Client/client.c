/*
** server.c for server minitalk in /home/wurfl_v/rendu/PSU_2013_minitalk
** 
** Made by a
** Login   <wurfl_v@epitech.net>
** 
** Started on  Thu Feb 27 14:28:59 2014 a
** Last update Sun Mar 23 13:24:40 2014 a
*/

#include <stdio.h>
#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

void	send_bin(int pid, char msg)
{
  usleep(1000);
  if (msg == '1')
    {
      if (kill(pid, SIGUSR1) == -1)
        {
          my_putstr("Error.\n");
          exit(0);
        }
    }
  else  if (msg == '0')
    {
      if (kill(pid, SIGUSR2) == -1)
        {
          my_putstr("Error.\n");
          exit(0);
        }
    }
}

void	trans_bin(char msg, int pid)
{
  int	i;

  i = 0;
  while (i < 8)
    {
      if ((msg >> i & 1) == 1)
	send_bin(pid, '1');
      else
	send_bin(pid, '0');
      i++;
    }
}

int	main(int argc, char **argv)
{
  int	a;
  char	*msg;
  int	x;

  x = 0;
  if (argc == 3)
    {
      a = my_getnbr(argv[1]);
      if (a == -1)
	{
	  exit(0);
	  return (0);
	}
      msg = argv[2];
      while (msg[x] != '\0')
	{
	  trans_bin(msg[x], a);
	  x++;
	}
    }
  else
    my_putstr("Wrong, bad arguments\n");
  return (0);
}
