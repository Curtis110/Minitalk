/*** client.c for client Minitalk in /home/wurfl_v/rendu/PSU_2013_minitalk
** 
** Made by a
** Login   <wurfl_v@epitech.net>
** 
** Started on  Mon Mar  3 19:47:54 2014 a
** Last update Sun Mar 23 03:11:28 2014 a
*/

#include <sys/types.h>
#include <signal.h>
#include <stdlib.h>

void		my_put_bin(int nb)
{
  static char	c;
  static int	i = 0;

  c = c + (nb << i);
  i++;
  if (i > 7)
    {
      if (c == 3)
	my_putchar('\n');
      else
	my_putchar(c);
      c = 0;
      i = 0;
    }
}

void	my_sig(int sign)
{
  if (sign == SIGUSR1)
    my_put_bin(1);
  if (sign == SIGUSR2)
    my_put_bin(0);
}

void	catch_signal()
{
  usleep(1000);
  if (signal(SIGUSR1, my_sig) == SIG_ERR)
    {
      my_putstr("Error.\n");
      exit(0);
    }
  if (signal(SIGUSR2, my_sig) == SIG_ERR)
    {
      my_putstr("Error.\n");
      exit(0);
    }
}

int	main(int argc, char **argv)
{
  pid_t	pid;

  pid = getpid();
  catch_signal();
  my_putstr("PID du server : ");
  my_put_nbr(pid);
  my_putchar('\n');
  while (42)
    pause();
}
