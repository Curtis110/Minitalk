##
## Makefile for Makefile minitalk in /home/wurfl_v/rendu/PSU_2013_minitalk
## 
## Made by a
## Login   <wurfl_v@epitech.net>
## 
## Started on  Tue Mar 18 16:40:54 2014 a
## Last update Thu Mar 20 21:58:36 2014 a
##

SRC_SERVER =	./Server/server.c \
		./Server/progs.c

SRC_CLIENT =	./Client/client.c \
		./Client/progs.c

OBJ_SERVER =	$(SRC_SERVER:.c=.o)

OBJ_CLIENT =	$(SRC_CLIENT:.c=.o)

RM =		rm -f

CC =		gcc

NAME_SERVER =	./Server/server

NAME_CLIENT =	./Client/client

all: 		$(NAME_SERVER)

$(NAME_SERVER):	$(OBJ_SERVER) $(OBJ_CLIENT)
		$(CC) $(OBJ_SERVER) -o $(NAME_SERVER) 
		$(CC) $(OBJ_CLIENT) -o $(NAME_CLIENT)

clean:
	$(RM) $(OBJ_SERVER) 
	$(RM) $(OBJ_CLIENT)

fclean: clean
	$(RM) $(NAME_SERVER) $(NAME_CLIENT)

re : fclean all

.PHONY: all clean fclean re
